<TEI xmlns="http://www.tei-c.org/ns/1.0" xmlns:h="http://www.w3.org/1999/xhtml">
  <teiHeader xml:lang="de">
    <fileDesc>
      <titleStmt>
        <title type="main">Datenmodell “eventSearch”</title>
        <author>
          <surname>Fritze</surname>
          <forename>Christiane</forename>
        </author>
        <author>
          <surname>Klug</surname>
          <forename>Helmut W.</forename>
        </author>
        <author ref="http://d-nb.info/gnd/13281899X">
          <surname>Kurz</surname>
          <forename>Stephan</forename>
        </author>
      </titleStmt>
      <publicationStmt>
        <authority>
          <orgName corresp="https://informationsmodellierung.uni-graz.at" ref="http://d-nb.info/gnd/1137284463">Zentrum für Informationsmodellierung -
                  Austrian Centre for Digital Humanities, Karl-Franzens-Universität Graz</orgName>
        </authority>
        <distributor>
          <orgName ref="https://gams.uni-graz.at">GAMS - Geisteswissenschaftliches Asset
                  Management System</orgName>
        </distributor>
        <availability>
          <licence target="https://creativecommons.org/licenses/by-nc/4.0">Creative Commons
                  BY-NC 4.0</licence>
        </availability>
        <date when="2020">2020</date>
        <pubPlace>Graz</pubPlace>
        <idno type="PID">o:konde.53</idno>
      </publicationStmt>
      <seriesStmt>
        <title ref="gams.uni-graz.at/konde">KONDE Weißbuch</title>
        <respStmt>
          <resp>Projektleitung</resp>
          <persName>
            <forename>Helmut</forename>
            <surname>Klug</surname>
          </persName>
        </respStmt>
        <respStmt>
          <resp>Datenmodellierung</resp>
          <persName>
            <forename>Selina</forename>
            <surname>Galka</surname>
          </persName>
        </respStmt>
      </seriesStmt>
      <sourceDesc>
        <p>Weißbuchartikel: Datenmodell “eventSearch”, born digital</p>
      </sourceDesc>
    </fileDesc>
    <encodingDesc>
      <projectDesc>
        <ab>
          <ref target="info:fedora/context:konde" type="context">KONDE Weißbuch</ref>
        </ab>
        <p>Im KONDE-Projekt, das aus Hochschulraumstrukturmitteln finanziert wird, beschäftigten
               sich sieben universitäre Partner und drei weitere Einrichtungen aus unterschiedlichen
               Blickwinkeln mit theoretischen und praktischen Aspekten der Digitalen Edition. Ein
               Outcome des Projektes stellt das Weißbuch dar, welches über 200 Artikel zum Thema
               Digitale Edition umfasst. Die behandelten Themenkomplexe reichen dabei über Digitale
               Editionswissenschaft im Allgemeinen, Annotation und Modellierung, Interfaces,
               Archivierung und Metadaten bis hin zu rechtlichen Aspekten.</p>
      </projectDesc>
    </encodingDesc>
  </teiHeader>
  <text xml:lang="de">
    <body>
      <div>
        <head>Datenmodell “eventSearch”</head>
        <div>
          <head>Fritze, Christiane; christiane.fritze@onb.ac.at / Klug, Helmut W.;
                  helmut.klug@uni-graz.at / Kurz, Stephan; stephan.kurz@oeaw.ac.at; Steindl,
                  Christoph; christoph.steindl@onb.ac.at </head>
          <p>Ein Event definiert sich – möglichst allgemein gehalten – als <hi rend="bold">was</hi> geschah <hi rend="bold">wann</hi> und mit <hi rend="bold">wem/was</hi>. In nahezu allen historischen, aber auch literarischen Quellen
                  sind Events überliefert. Ein einheitliches Datenmodell für die <ref target="/o:konde.17" type="internal">Annotation</ref> von Events würde den
                  Aufbau einer Eventdatenbank/-Schnittstelle vorantreiben, mithilfe derer
                  Ereignisse, die in edierten Quellen überliefert und annotiert sind,
                  zusammengeführt werden können. Ein prototypisches Anwendungsbeispiel ist <ref target="https://correspsearch.net" type="external">correspSearch</ref>.</p>
          <p>Da sich die <ref target="/o:konde.178" type="internal">TEI</ref> als de
                  facto-Standard für die <ref target="/o:konde.137" type="internal">Modellierung</ref> von historischen Texten herauskristallisiert hat, und die
                  TEI-Community die Annotation von Events immer wieder diskutiert, wurde für
                  Eventdaten das nachstehende Modell entwickelt, das sich grundlegend an den
                  Vorgaben der TEI für Namen, Daten, Personen und Orte <bibl>(TEI Guidelines, Ch.
                     13)</bibl> orientiert, dabei aber auch neue Wege beschreitet. Das <hi rend="italic">Codesnippet</hi> zeigt einen möglichst vollständig annotierten
                  Eintrag <code>&lt;event&gt;</code> im Abschnitt <code>&lt;listEvent&gt;</code> des
                     <code>&lt;teiHeader&gt;</code>; die in Blockbuchstaben geschriebenen Attribute
                  sind im TEI-<ref target="/o:konde.166" type="internal">Schema</ref> noch nicht
                  vorgesehen und müssen zur Zeit durch die Anpassung der Schemata ermöglicht
                  werden:</p>
          <p>
            <code ana="block">&lt;event
    att.datable.*="1950-05-05"
    where="placeName"
    ATT.DURATION.*="duration.w3c [e.g. PT45M]"
    xml:id="some_ID"
    source="URI"
    type="Type of the event"
    &lt;head&gt;[short title]&lt;/head&gt;
    &lt;label type="generated"&gt;[What happened.]&lt;/label&gt;
    &lt;desc&gt;&lt;!-- Further information as you like/need and as
    is TEI compliant. May contain further linking between 
    entities involved in the event. --&gt;&lt;/desc&gt;
&lt;/event&gt;</code>
          </p>
          <p>Für die kürzeste Variante ein Event zu beschreiben reicht dieser Code:</p>
          <p>
            <code ana="block">&lt;event when-iso="1950-05-05"&gt;
    &lt;label&gt;[What happened.]&lt;/label&gt;
&lt;/event&gt;</code>
          </p>
          <p>Das für eine einheitliche Beschreibung von Events konzipierte Datenmodell baut
            also weitestgehend auf den Vorgaben der TEI auf (ein Beispiel einer Edition, die <code>&lt;event&gt;</code>s nach <ref target="/o:konde.178" type="internal">TEI</ref> <ref target="https://tei-c.org/Vault/P5/3.6.0/" type="external">P5 3.6.0</ref> für die Verlistung von Sitzungsdaten und Tagesordnungen verwendet: <ref target="/o:konde:pXXXMPR">Edition der Ministerratsprotokolle</ref>). Eine Erweiterung der TEI wird
                  aber angestrebt und in einem Kooperationsprojekt von ÖNB, ÖAW und ZIM
                  vorangetrieben:</p>
          <list>
            <item>Ergänzung des <code>&lt;event&gt;</code>-Elements um das Attribut <code ana="https://tei-c.org/release/doc/tei-p5-doc/en/html/ref-att.duration.w3c.html">@dur</code>/<code ana="https://tei-c.org/release/doc/tei-p5-doc/en/html/ref-att.duration.iso.html">@dur-iso</code>
            </item>
            <item>Einführung des Elements <code>&lt;eventName&gt;</code>
            </item>
            <item>Vereinheitlichung des Datenmodells für <code>&lt;event&gt;</code>,
                        <code>&lt;person&gt;</code>, <code>&lt;org&gt;</code>,
                        <code>&lt;place&gt;</code>
            </item>
          </list>
          <div type="literature">
            <listBibl>
              <biblStruct corresp="http://zotero.org/groups/1332658/items/QAQ2BTWP" type="journalArticle" xml:id="Dumont2016">
                <analytic>
                  <title level="a">correspSearch – Connecting Scholarly Editions of
                              Letters</title>
                  <author>
                    <forename>Stefan</forename>
                    <surname>Dumont</surname>
                  </author>
                </analytic>
                <monogr>
                  <title level="j">Journal of the Text Encoding Initiative</title>
                  <imprint>
                    <biblScope unit="issue">10</biblScope>
                    <date>2016</date>
                    <note type="url">http://journals.openedition.org/jtei/1742</note>
                  </imprint>
                </monogr>
              </biblStruct>
              <biblStruct corresp="http://zotero.org/groups/1332658/items/PM2TEXGP" type="webpage" xml:id="TEI_Consortiumg">
                <analytic>
                  <title level="a">13 Names, Dates, People, and Places</title>
                  <author>
                    <name>TEI Consortium</name>         
         </author>
                </analytic>
                <monogr>
                  <title level="m">The TEI P5 Guidelines</title>
                  <imprint>
                    <date/>
                    <note type="accessed">2020-01-21T16:06:55Z</note>
                    <note type="url">https://tei-c.org/release/doc/tei-p5-doc/en/html/ND.html</note>
                  </imprint>
                </monogr>
              </biblStruct>
              <biblStruct corresp="http://zotero.org/groups/1332658/items/R6AHPM4P" type="webpage" xml:id="Fritze2019b">
                <analytic>
                  <title level="a">Recreating history through events</title>
                  <author>
                    <forename>Christiane</forename>
                    <surname>Fritze</surname>
                  </author>
                  <author>
                    <forename>Helmut W.</forename>
                    <surname>Klug</surname>
                  </author>
                  <author>
                    <forename>Stephan</forename>
                    <surname>Kurz</surname>
                  </author>
                  <author>
                    <forename>Christoph</forename>
                    <surname>Steindl</surname>
                  </author>
                </analytic>
                <monogr>
                  <title level="m">zenodo.</title>
                  <imprint>
                    <date>2019</date>
                    <note type="accessed">2020-05-07</note>
                    <note type="url">https://zenodo.org/record/3447298#.XrPOkBMzbUI</note>
                  </imprint>
                </monogr>
              </biblStruct>
              <biblStruct corresp="http://zotero.org/groups/1332658/items/RMNU8DGW" type="book" xml:id="Schöch">
                <monogr>
                  <title level="m">DHd 2020. Book of Abstracts: Spielräume. Digital
                              Humanities zwischen Modellierung und Interpretation.</title>
                  <idno type="ISBN">978-3-945437-07-0</idno>
                  <editor>
                    <forename>Christof</forename>
                    <surname>Schöch</surname>
                  </editor>
                  <imprint>
                    <date/>
                    <note type="url">https://doi.org/10.5281/zenodo.3666690</note>
                  </imprint>
                </monogr>
              </biblStruct>
            </listBibl>
          </div>
          <div type="subsection" xml:id="links">
            <list>
              <item>
                <ref target="/o:konde.17" type="internal">Annotation</ref>
              </item>
              <item>
                <ref target="/o:konde.178" type="internal">TEI</ref>
              </item>
              <item>
                <ref target="/o:konde.137" type="internal">Modellierung</ref>
              </item>
              <item>
                <ref target="/o:konde.133" type="internal">CIDOC CRM</ref>
              </item>
              <item>
                <ref target="/o:konde.42" type="internal">CMIF</ref>
              </item>
            </list>
          </div>
          <div type="subsection" xml:id="projects">
            <list>
              <item>
                <ref target="https://correspsearch.net" type="external">correspSearch</ref>
              </item>
              <item>
                <ref target="https://labs.onb.ac.at/gitlab/digital-editions/eventSearch" type="external">ONB
                           Gitlab: Eventsearch</ref>
              </item>
            </list>
          </div>
          <div type="subsection" xml:id="topics">
            <list>
              <item>Annotation und Modellierung</item>
            </list>
          </div>
        </div>
      </div>
    </body>
  </text>
</TEI>